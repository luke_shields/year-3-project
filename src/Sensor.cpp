#include "Sensor.h"

Sensor::Sensor(int pin){
  _pin = pin;

  pinMode(_pin, INPUT);
}

int Sensor::GetAnalogRead(){
  return analogRead(_pin);
}

bool Sensor::GetDigitalRead(){
  return digitalRead(_pin);
}
