#include <Arduino.h>
#include <ArduinoBLE.h> // Library from 
#include <mbed.h>
#include <Wire.h>
#include "LinkedList.hpp"
#include "Encoder.h"
#include "Motor.h"
#include "PinDefinitions.h"
#include "Sensor.h"
#include "Repeater.h"
#include "PIDController.h"
#include "MotorController.h"
#include "IMU.h"

#include "Globals.h"



// Constants
#define GYR_MULTIPLIER 1.14459
#define FSM_UPDATE_PERIOD_MS 15
#define MAPPING_SPEED 250

// Global Objects
CarState currCarState = Idle; // Enum containing current state of FSM
TelemetryData currTelemetryData; // Struct containing most recently collected data from all sensors
Coordinate currCoordinate; // Struct containing car's current coordinate and direction

Sensor LineSensor(LINE_SNSR);
Sensor Potentiometer(POT_SNSR);
IMU ModifiedIMU(Wire1);

Encoder Enc(ENC_CHANA, ENC_CHANB);
Motor Mot(MOT_ENABLE, MOT_PHASE, MOT_MODE1, MOT_MODE2, MOT_NSLEEP, MOT_NFAULT);
PIDController PID(0.5f, 0.01f, 0.0f, 255.0f, 0.0f, FSM_UPDATE_PERIOD_MS);
MotorController MotController(&Mot, &PID, &Enc);

Repeater semaphorRepeater; // Handles the periodic release of the semaphore 
rtos::Semaphore FSMSemaphore(0, 1);

BLEService NanoService("19B10000-E8F2-537E-4F6C-D104768A1214"); // BLE Nano Service
BLEByteCharacteristic carStateCharacteristic("00000000-0000-0000-0000-000000000001", BLEWrite); // Characteristic to change car's state

//Global Variables
int LapCounter = 0; // Stores current number of laps completed
bool InSectionMarker = 0; // Stores if the cas is currently over a white marker
bool BLECarStateCharacteristicUpdated = false; // Indicate a BLE characterstic has been updated
bool RecordLapTimes = false; // Indicates if lap times should be recorded



LinkedList<TelemetryData> TrackTelemetries; // Stores all Telemetry data collected during one lap from mapping mode
LinkedList<Coordinate> TrackCoordinates; // Stores coordinates from periodic intervals around the track
LinkedList<PositionSpeed> PositionSpeeds; // Stores appropriate speed to be travelling for each position on the track given as pulses from the start line
LinkedList<LapTime> LapTimes; // Stores collected Lap times











// Functions
void ReleaseSemaphore();
void UpdateSectionMarkers();
void UpdateTelemetryData();
int GetDesiredSpeed(int position);
int GetPosition();
int CalculateSpeedForIndex(int index, int prevSpeed);
void UpdateCurrCoordinate(TelemetryData prevTelemetryData);
void CarStateCharacteristicUpdated(BLEDevice central, BLECharacteristic characteristic);
int ReturnToStartLineSpeed();

void setup() {
  Serial.begin(115200);
  Serial.println("Serial Connected");
  
  while(!ModifiedIMU.begin()); // Waits for IMU to be ready

  semaphorRepeater.Attach(ReleaseSemaphore, FSM_UPDATE_PERIOD_MS); // Sets up semaphore release with period FSM_UPDATE_PERIOD_MS

  MotController.SetEnable(true);
  TrackCoordinates.Clear();
  TrackTelemetries.Clear();

  Coordinate startPosition;
  startPosition.XPos = 0;
  startPosition.YPos = 0;
  TrackCoordinates.Append(startPosition);

  
  if (!BLE.begin()) {
    // Ensures BLE is ready
    Serial.println("starting BLE failed!");

    while (1);
  }
  

  BLE.setLocalName("Luke's Nano");
  BLE.setAdvertisedService(NanoService);

  // Add the characteristic to the service
  NanoService.addCharacteristic(carStateCharacteristic);

  // Add service
  BLE.addService(NanoService);

  // Set the initial value for the characeristic:
  carStateCharacteristic.writeValue(0);
  // Sets CarStateCharacteristicUpdated() to be run if carStateCharacteristic update
  carStateCharacteristic.setEventHandler(BLEUpdated, CarStateCharacteristicUpdated);

  // start advertising
  BLE.advertise();
  

}

void loop() {
  // Connects to BLE central if available
  BLEDevice central = BLE.central();

  // Checks if Semaphore if available
  if(FSMSemaphore.try_acquire() == true){
    // Loop iterates with period FSM_UPDATE_PERIOD_MS
    switch (currCarState){
      case(Idle):{
        /*  Car is in Idle state
            During this state the car will wait for further state changes via BLE
            It will ensure state changes are valid, and car has correct variables set for the desired state
        */
        LapCounter = 0;
        RecordLapTimes = false;
        MotController.ApplyBrakes();
        digitalWrite(LED_BUILTIN, LOW);
        
        if(BLECarStateCharacteristicUpdated == true){
          // Run if user has changed desired car state via BLE
          BLECarStateCharacteristicUpdated = false;

          // Ensure set state is valid
          CarState BLEDesiredCarState = CarState(carStateCharacteristic.value());

          switch(BLEDesiredCarState){
            case Mapping:{
              // State has been set to mapping, wipes all previous learnt data
              TrackTelemetries.Clear();
              PositionSpeeds.Clear();
              TrackCoordinates.Clear();

              currCarState = Mapping;
              break;
            }

            case Racing:{
              if(TrackTelemetries.getLength() > 0){
                // Ensures track has been learned
                currCarState = Racing;
              }
              break;
            }

            case Debugging:{
              currCarState = Debugging;
            }
          }

        }
        
        break;
      }
      case Mapping:{
        /*  Car is in Mapping state
            During this state the car will populate the TrackTelemetries, TrackCoordinates and PositionSpeeds LinkedLists
            It will do this through completing 3 laps
            The first lap ensures the car is travelling at the correct speed when it passes the start line for the second lap
            The second lap stores the current TelemetryData for each position in TrackTelemetries, and the current coordinate in TrackCoordinates
            The third lap takes the car around to directly before the start line, where it stops and computes the speeds for PositionSpeeds.
            The car is then put into Idle state

        */
        TelemetryData prevTelemetryData = currTelemetryData; 

        UpdateTelemetryData(); // Update currTelemetryData
        UpdateCurrCoordinate(prevTelemetryData); // Update currCoordinate
        UpdateSectionMarkers(); // Handle detection of section markers, updating LapCounter where required
        

        if(LapCounter < 2){
          // Travels at fixed speed without storing telemetries until a full lap has been complete
          MotController.SetSpeed(MAPPING_SPEED, FSM_UPDATE_PERIOD_MS);
        } else if(LapCounter == 2){
          // Travels at fixed speed storing both telemetries and coordinates
          TrackTelemetries.Append(currTelemetryData); // Adds current telemetry data
          TrackCoordinates.Append(currCoordinate); // Adds current coordinate

          MotController.SetSpeed(MAPPING_SPEED, FSM_UPDATE_PERIOD_MS);

        } else if (LapCounter == 3){
          // Track has been learnt so car should return to the start line
          //  Return to the start line using ReturnToStartLineSpeed() function
          int returnToStartLineSpeed = ReturnToStartLineSpeed(); 
          if(returnToStartLineSpeed != -1){
            // Car hasn't reached the start line
            MotController.SetSpeed(returnToStartLineSpeed, FSM_UPDATE_PERIOD_MS);
          } else {
            // Car has reached the start line
            while(!MotController.ApplyBrakes()){};

            PositionSpeeds.moveToStart();
            TrackTelemetries.moveToStart();

            for(int i = 0; i < TrackTelemetries.getLength(); i++){
              // Loop initiates PositionSpeeds LinkedList with positions taken from TrackTelemetries
              struct PositionSpeed newPositionSpeed;
              newPositionSpeed.Speed = MAPPING_SPEED; // Set default speed for each position to MAPPING_SPEED, will be overwritten later
              newPositionSpeed.Position = TrackTelemetries.getCurrent().Pulses;
              PositionSpeeds.Append(newPositionSpeed);
              if(TrackTelemetries.next() ==false){
                // End of TrackTelemetries has been reached
                break;
              }
            } 

            PositionSpeeds.moveToStart();
            TrackTelemetries.moveToStart();
            
            for(int i = 0; i < PositionSpeeds.getLength(); i++){
              // Loop iterates through all positions in PositionSpeeds, assigning the correct speed to each
              int prevSpeed = PositionSpeeds.getCurrent().Speed; // Store speed of previous position

              if(PositionSpeeds.next() == false){
                // End of PositionSpeeds has been reached
                break;
              }  

              int newSpeed = CalculateSpeedForIndex(i, prevSpeed); // Calculates appropriate speed for current position
              PositionSpeeds.getCurrent().Speed = newSpeed;
            }

            LapCounter = 0; // Reset LapCounter
            currCarState = Debugging; // Return to Idle state 
          }
          
        }
        
        break;
      }
      case Racing:{
        /*  Car is in Racing state
            During this state the car will first travel at a fixed speed until the start line has been detected
            The car will then update its current position on the track, before setting the correct speed according to PositionSpeeds
            This will be done until the car has completed 20 laps or it receives a BLE command to change state
            In either event the car will return to the start line before switching to Idle state
        */

        TelemetryData prevTelemetryData = currTelemetryData;
        UpdateTelemetryData(); // Update currTelemetryData
        UpdateCurrCoordinate(prevTelemetryData); // Update currCoordinate
        UpdateSectionMarkers(); // Handle detection of section markers, updating LapCounter where required

        if(BLECarStateCharacteristicUpdated == true){
          // Check to see if BLE command has been received
          BLECarStateCharacteristicUpdated = false; 
          LapCounter = -1; // Used to indicate car should exit Racing state when car has reached the start line
        }

        if(LapCounter == 0){
          // The car hasn't yet detected the start line so should travel at a fixed speed
          digitalWrite(LED_BUILTIN, HIGH);
          MotController.SetSpeed(300, FSM_UPDATE_PERIOD_MS);
        } else if (LapCounter == -1){
          // Indicates a state change is desired therefore car should return to the start line
          int returnToStartLineSpeed = ReturnToStartLineSpeed();
          if(returnToStartLineSpeed != -1){
            // Start line hasn't been reached
            MotController.SetSpeed(returnToStartLineSpeed, FSM_UPDATE_PERIOD_MS);
          } else {
            // Start line has been reached
            currCarState = Idle;
          }
        } else {
          // The car should race as normal
          RecordLapTimes = true;
          MotController.SetSpeed(GetDesiredSpeed(GetPosition()), FSM_UPDATE_PERIOD_MS); // Calculates current position, and set speed accordingly
        } 

        break;
      }
      case Debugging:{
        MotController.ApplyBrakes();
        Serial.flush();
        while(!Serial.available());
        
        // For debuging only
        TrackCoordinates.moveToStart();
        TrackTelemetries.moveToStart();
        PositionSpeeds.moveToStart();
        /*
        do {
          Serial.print(TrackTelemetries.getCurrent().LineSensor);
          Serial.print(", ");
          Serial.print(TrackTelemetries.getCurrent().Pulses);
          Serial.print(", ");
          Serial.print(TrackTelemetries.getCurrent().PotentiometerSensor);
          Serial.print(", ");

          Serial.print(TrackTelemetries.getCurrent().AccX);
          Serial.print(", ");
          Serial.print(TrackTelemetries.getCurrent().AccY);
          Serial.print(", ");
          Serial.print(TrackTelemetries.getCurrent().AccZ);
          Serial.print(", ");

          Serial.print(TrackTelemetries.getCurrent().GyrX);
          Serial.print(", ");
          Serial.print(TrackTelemetries.getCurrent().GyrY);
          Serial.print(", ");
          Serial.print(TrackTelemetries.getCurrent().GyrZ);
          Serial.print(", ");

          Serial.print(TrackTelemetries.getCurrent().MagX);
          Serial.print(", ");
          Serial.print(TrackTelemetries.getCurrent().MagY);
          Serial.print(", ");
          Serial.print(TrackTelemetries.getCurrent().MagZ);

          // For debuging only
          Serial.print(", ");
          Serial.println(TrackTelemetries.getCurrent().timeStamp);
        } while (TrackTelemetries.next());
        */
        /*
        do {
          Serial.print(PositionSpeeds.getCurrent().Position);
          Serial.print(", ");
          Serial.println(PositionSpeeds.getCurrent().Speed);
        } while (PositionSpeeds.next());
        */
        /*
        do { 
          Serial.print(TrackCoordinates.getCurrent().XPos);
          Serial.print(",");
          Serial.println(TrackCoordinates.getCurrent().YPos);
        } while(TrackCoordinates.next());
        */

        do { 
          Serial.print(TrackCoordinates.getCurrent().XPos);
          Serial.print(",");
          Serial.print(TrackCoordinates.getCurrent().YPos);
          Serial.print(",");
          Serial.println(PositionSpeeds.getCurrent().Speed);
        } while(TrackCoordinates.next() && PositionSpeeds.next());

        /*
        do { 
          Serial.print(LapTimes.getCurrent().finishTime);
          Serial.print(",");
          Serial.println(LapTimes.getCurrent().totalTime);
        } while(LapTimes.next());
        */
        while(1);
        break;
      }
    }
  }

}

void UpdateSectionMarkers(){
  /*  This function handles section marker events
      This includes updating the LapCounter when necessary, and reseting the relevant variables when a lap is complete
  */

  if(currTelemetryData.LineSensor == true){
    // Detected section marker
    if(InSectionMarker == false){
      // Entered a marker
      InSectionMarker = true;

      if(LapCounter == 0){
        // Has not completed a lap therefore section marker is taken to be the start line
        LapCounter++;

        // Resets relevant variables
        Enc.Reset(); // Resets encoder pulses
        currTelemetryData.Pulses = 0;
        currCoordinate.Direction = 0;
        currCoordinate.XPos = 0;
        currCoordinate.YPos = 0;

        // Moves linked lists to start
        PositionSpeeds.moveToStart();
        TrackTelemetries.moveToStart();

      } else if (sqrt(sq(currCoordinate.XPos) + sq(currCoordinate.YPos)) < 25){
        // Ensures the detected section marker is the start line
        // Indicates lap has been completed
        LapCounter++;

        if(RecordLapTimes == true){
          // Stores the lap time if desired
          struct LapTime newLapTime;
          newLapTime.finishTime = millis();
          newLapTime.totalTime = newLapTime.finishTime - LapTimes.Last().finishTime;
          LapTimes.Append(newLapTime);
        }

        // Resets relevant variables
        Enc.Reset();
        currTelemetryData.Pulses = 0;
        currCoordinate.Direction = 0;
        currCoordinate.XPos = 0;
        currCoordinate.YPos = 0;

        // Moves linked lists to start
        PositionSpeeds.moveToStart();
        TrackTelemetries.moveToStart();
      }
    }
  } else {
    // Section marker no longer detected
    InSectionMarker = false;
  }
}

int ReturnToStartLineSpeed(){
  /*  This function aids in returning the car to the start line
      This is done through returning a speed value that should be set in order to return the car to the start line

      When the car has reached the start line the function returns -1
  */
  float slowDownPoint = 300.0; // This is the number of pulses away from the finish line that the car should start to slow down
  int minSpeedPoint = 50; // This is the number of pulses away from the finish line that the car should be travelling at its minimum speed
  int stopPoint = 20; // This is the number of pulses away from the finish line that the car should stop
  if(currTelemetryData.Pulses < TrackTelemetries.Last().Pulses - slowDownPoint){
    // Car is far away from the finish line so should travel at MAPPING_SPEED
    return MAPPING_SPEED;
  } else if (currTelemetryData.Pulses < TrackTelemetries.Last().Pulses - minSpeedPoint){
    // Car should travel at speed according to how many pulses it is away from the minimum speed point
    return (((TrackTelemetries.Last().Pulses - currTelemetryData.Pulses - minSpeedPoint) / slowDownPoint) * (MAPPING_SPEED - 50) + 50);
  } else if (currTelemetryData.Pulses < TrackTelemetries.Last().Pulses - stopPoint){
    // Car should travel at its minimum speed
    return 50;
  } else {
    // The finish line has been reached
    return -1;
  }
}

void UpdateCurrCoordinate(TelemetryData prevTelemetryData){
  /*  This function handles updating the currCoordinate variable
      This cannot be called before UpdateTelemetryData() in a single FSM update
      This is passed the prevTelemetryData taken from the previous update of the FSM
  */
  Coordinate prevCoordinate = currCoordinate;
  int currPulsesDelta = currTelemetryData.Pulses - prevTelemetryData.Pulses; // Calculates distance travelled in encoder pulses
  float currAngularChange = currTelemetryData.GyrZ * FSM_UPDATE_PERIOD_MS * 0.001; // Calculate angular change

  // Calculates current X and Y values according to previous location
  currCoordinate.XPos = prevCoordinate.XPos - currPulsesDelta*sin(radians(GYR_MULTIPLIER*(prevCoordinate.Direction + currAngularChange))); 
  currCoordinate.YPos = prevCoordinate.YPos + currPulsesDelta*cos(radians(GYR_MULTIPLIER*(prevCoordinate.Direction + currAngularChange)));

  currCoordinate.Direction = prevCoordinate.Direction + currAngularChange;
}

void ReleaseSemaphore(){
  /*  This function handles the release of the semaphore that controls the FSM update rate
      This should be called regularly with a period of FSM_UPDATE_PERIOD_MS
  */
  FSMSemaphore.release();
}

void CarStateCharacteristicUpdated(BLEDevice central, BLECharacteristic characteristic){
  /*  This function handles indicating to the FSM that a BLE event has occured
      This is called whenever a command is received over BLE to change the car's state
  */
  BLECarStateCharacteristicUpdated = true;
}

void UpdateTelemetryData(){
  /*  This function handles the updating the currTelemetryData variable
      This includes collecting data from all available sensors
  */
  TelemetryData prevTelemetryData = currTelemetryData;
  float AccEMAAlpha = 0.1;
  

  currTelemetryData.LineSensor = LineSensor.GetDigitalRead();
  currTelemetryData.PotentiometerSensor = Potentiometer.GetAnalogRead();
  currTelemetryData.Pulses = Enc.GetPulses();

  
  if(ModifiedIMU.accelerationAvailable()){
    // Ensures accelerometer is read to be read from
    ModifiedIMU.readAcceleration(currTelemetryData.AccX, currTelemetryData.AccY, currTelemetryData.AccZ);

    // Applies an exponential moving average to the raw accelerometer readings
    currTelemetryData.AccX = currTelemetryData.AccX * AccEMAAlpha + (1 - AccEMAAlpha) * prevTelemetryData.AccX;
    currTelemetryData.AccY = currTelemetryData.AccY * AccEMAAlpha + (1 - AccEMAAlpha) * prevTelemetryData.AccY;
    currTelemetryData.AccZ = currTelemetryData.AccZ * AccEMAAlpha + (1 - AccEMAAlpha) * prevTelemetryData.AccZ;
  }

  if(ModifiedIMU.gyroscopeAvailable()){
    // Ensures the gyroscope is ready to be read from
    ModifiedIMU.readGyroscope(currTelemetryData.GyrX, currTelemetryData.GyrY, currTelemetryData.GyrZ);
  }

  if(ModifiedIMU.magneticFieldAvailable()){
    // Ensures the magnetometer is ready to be read from
    ModifiedIMU.readMagneticField(currTelemetryData.MagX, currTelemetryData.MagY, currTelemetryData.MagZ);
  }

  currTelemetryData.timeStamp = millis(); 
}


int GetPosition(){
  /*  This function returns the current position from PositionSpeeds that the car is at
      This uses the current pulses detected and finds a close position for which a speed would have been calculated
  */
  while (TrackTelemetries.getCurrent().Pulses < currTelemetryData.Pulses){
    // This loops through TrackTelemetries to find the reading that just exceeds the current pulses
    // This does not loop through all TrackTelemetries, just the positions since the last FSM update
    if(TrackTelemetries.next() == false){
      // Means has reached end of linkedlist without finding suitable speed position
      // Indicates the current number of pulses exceeds the number recording during mapping lap
      return TrackTelemetries.Last().Pulses; // Returns final position of mapping lap
    }
  } 
  return TrackTelemetries.getCurrent().Pulses; // Returns the pulses from the relevant reading of the learning lap
}

int GetDesiredSpeed(int position){
  /*  This function returns the speed from PositionsSpeeds that the car should travel at when passed in the 'position'
  */
  while (PositionSpeeds.getCurrent().Position < position){
    if(PositionSpeeds.next() == false){
      // Means has reached end of linkedlist without finding suitable speed position
      // Indicates the current number of pulses exceeds the number recording during mapping lap
      return PositionSpeeds.Last().Speed; // Returns the final speed of PositionSpeeds
    }
  } 

  return PositionSpeeds.getCurrent().Speed; // Returns the correct speed for the passed in 'position'

}

int CalculateSpeedForIndex(int index, int prevSpeed){
  /*  This function handles the calculation of the correct speed for a given index of the TrackTelemetries linked list
      The function is passed in this 'index' as well as the speed calculated for the index just prior
      The funciton then returns this calculated value
  */
  TrackTelemetries.moveToStart();
  int focalPoint = int(prevSpeed / MAPPING_SPEED); // Calculates the focal point depending on the car's speed
  float iterationMult = 0.9; // For storing how the multiplier should change each iteration
  float weightedInverse = 0.0; // For storing the weighted inverse of gyrz
  float gyrZSum = 0.0; // For storing the current GyrZ sum
  int sumMax = 1000; // For storing the maximum value gyrZSum should reach
  int numInSum = 0; // For storing the current number of values in the sum

  

  for(int i = 0; i < index; i++){
    // Moves the linked list to the correct index
    TrackTelemetries.next();
  }

  while(gyrZSum < sumMax){
    // This loop iterates until the gyrZSum exceeds the sumMax
    if(TrackTelemetries.getCurrent().GyrZ > 1){
      // WeightedInverse calculated for GyrZ readings greater than 1
      weightedInverse += abs(1/TrackTelemetries.getCurrent().GyrZ) * pow(iterationMult, abs(focalPoint - numInSum));
    } else {
      // Ensures near infinite weighted inverses aren't created by near 0 GyrZ readings
      weightedInverse += 1 * pow(iterationMult, abs(focalPoint - numInSum));
    }

    gyrZSum += abs(TrackTelemetries.getCurrent().GyrZ); // Adds current GyrZ to sum
    numInSum++;
    if(TrackTelemetries.next() == false){
      // Has reach the finish line and should start taking values from the start of the track as its a loop
      TrackTelemetries.moveToStart();
    }
  }

  weightedInverse = weightedInverse * (sumMax / gyrZSum); // For cases where a single GyrZ readings made gyrZSum exceed sumMax dramatically
  
  // Used to set weightedInverse to an appropriate range for the car's speed
  float tempOverallMult = 20; 
  float tempOffset = 300;

  int speed = (weightedInverse * tempOverallMult) + tempOffset;
  return speed;
}

