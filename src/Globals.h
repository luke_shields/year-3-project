#ifndef GLOBALS_H
#define GLOBALS_H
struct TelemetryData{
  bool LineSensor;
  int PotentiometerSensor;
  unsigned int Pulses;
  float AccX, AccY, AccZ;
  float GyrX, GyrY, GyrZ; 
  float MagX, MagY, MagZ; 

  // For debugging only
  unsigned long timeStamp;
};

struct Coordinate{
  float XPos;
  float YPos;
  float Direction;
};

struct PositionSpeed{
  int Position;
  int Speed;
};

struct LapTime{
  int finishTime;
  int totalTime;
};

enum CarState{
  Idle,
  Mapping,
  Racing,
  Debugging

};
#endif