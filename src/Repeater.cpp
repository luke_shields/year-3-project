#include "Repeater.h"
#include <Arduino.h>

Repeater::Repeater(){}

void Repeater::Attach(void (*f)(), unsigned long period_ms){
    _func = f;
    _period_ms = period_ms;
    _repeaterThread = new rtos::Thread(update, this);
}

void Repeater::update(void const *argument){
    Repeater* self = (Repeater*)argument;
    while(1){
        // This loops forever
        self->_func(); // Calls the function passed to Attach()
        rtos::ThisThread::sleep_for(self->_period_ms); // _repeaterThread sleeps for _period_ms
        

    }

    
    
}

Repeater::~Repeater()
{
}