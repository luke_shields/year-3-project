#include "PIDController.h"
#include <Arduino.h>
PIDController::PIDController(float Kp, float Ki, float Kd, float maxVal, float minVal, float deltaT_ms)
    : _Kp(Kp), _Ki(Ki), _Kd(Kd), _maxVal(maxVal), _minVal(minVal), _deltaT_ms(deltaT_ms)
{
    _prevError = 0;
    _currError = 0;
    _output = 0;
}
void PIDController::CalcOutput(float desiredVal, float currentVal)
{
    
    float prev2Error = _prevError;
    _prevError = _currError;
    _currError = desiredVal - currentVal;


    float prevOutput = _output;

    _output = prevOutput - (_prevError * _Kp) + (_currError * _Kp) + _Ki * (_currError * _deltaT_ms) + (_Kd / _deltaT_ms) * (_currError - (2 * _prevError) + prev2Error);

    

    if (_output > _maxVal)
    {
        _output = _maxVal;
    }
    else if (_output < _minVal)
    {
        _output = _minVal;
    }

}
void PIDController::SetKp(float kp)
{
    _Kp = kp;
}
void PIDController::SetKi(float ki)
{
    _Ki = ki;
}
void PIDController::SetKd(float kd)
{
    _Kd = kd;
}
void PIDController::ResetController(){
    _prevError=0;
    _currError=0;
    _output=0;
}
float PIDController::GetKp()
{
    return _Kp;
}
float PIDController::GetKi()
{
    return _Ki;
}
float PIDController::GetKd()
{
    return _Kd;
}
float PIDController::GetControlAction()
{
    return _output;
}
