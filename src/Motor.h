#ifndef MOTOR_H
#define MOTOR_H

#include <Arduino.h>
class Motor{
  private:
  // Pins
  int _enablePin;
  int _phasePin;
  int _mode1Pin;
  int _mode2Pin;
  int _nSleepPin;
  int _nFaultPin;

  // Variables
  bool _isEnabled;
  int _pwm;
  bool _direction;
  bool _brakingMode;
  bool _sleepMode;

  
  public:
  Motor(int enablePin, int phasePin, int mode1Pin, int mode2Pin, int nSleepPin, int nFaultPin);

  // Setters
  void SetEnable(bool isEnabled); 
  
  void SetPWM(int pwm);
  
  void SetDirection(bool direction);

  void SetBrakingMode(bool applyBraking);

  void SetSleepMode(bool isSleepMode);


  

  // Setters

  bool GetEnable();

  bool GetDirection();

  bool GetBrakingMode();

  bool GetSleepMode();

  byte GetPWM();
  


  
};



#endif
