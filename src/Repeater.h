

#ifndef REPEATER_H
#define REPEATER_H

#include <Arduino.h>
#include <mbed.h>
#include <rtos.h>


class Repeater{
    private:
        unsigned long _period_ms = 1000;  // The period of the Repeater in milliseconds
        std::function<void()> _func; // For storing the function that is attached
        rtos::Thread* _repeaterThread; // The thread that will be calling the attached function via update()
        
        static void update(void const *argument); // The function called by _repeaterThread, is passed in the Repeater object

    public:
        Repeater();
        void Attach(void(*f)(), unsigned long period_ms); // This function is passed the function to be repeater and a period
        ~Repeater();

};

#endif