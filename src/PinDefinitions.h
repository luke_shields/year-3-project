#ifndef PINDEFINITIONS_H
#define PINDEFINITIONS_H

// Motor Pins
#define MOT_ENABLE 10
#define MOT_PHASE 9
#define MOT_MODE1 8
#define MOT_MODE2 7
#define MOT_NSLEEP 6
#define MOT_NFAULT 5

// Encoder Pins
#define ENC_CHANA 2
#define ENC_CHANB 3

// Line Sensor Pin
#define LINE_SNSR 15

// Potentiometer Pin
#define POT_SNSR 14

#endif
