#include "Encoder.h"
Encoder::Encoder(int channelAPin, int channelBPin) : _chanAPin(channelAPin), _chanBPin(channelBPin){
        pinMode(_chanAPin, INPUT);
        pinMode(_chanBPin, INPUT);

        attachInterrupt(digitalPinToInterrupt(_chanAPin), ChanAISR, CHANGE); // Calls ChanAISR when _chanAPin changes 
        attachInterrupt(digitalPinToInterrupt(_chanBPin), ChanBISR, CHANGE);  // Calls ChanBISR when _chanBPin changes 
}

void Encoder::ChanAISR(){
  _pulses++;
}

void Encoder::ChanBISR(){
  _pulses++;
}

int Encoder::GetPulses(){
  return _pulses;
}

void Encoder::Reset(){
  _pulses = 0;
}

unsigned int Encoder::_pulses = 0;



