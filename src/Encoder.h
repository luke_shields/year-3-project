#ifndef ENCODER_H
#define ENCODER_H

#include <Arduino.h>
#include "Encoder.h"


class Encoder{
  private:
  byte _chanAPin;
  byte _chanBPin;
  static unsigned int _pulses;
  
  public:
  Encoder(int channelAPin, int channelBPin);

  static void ChanAISR();

  static void ChanBISR();

  static int GetPulses();

  static void Reset();

  
  
};
#endif
